# cube-md

1. Create .ENV file, from .ENV.EXAMPLE
2. Set DB_NAME, DB_USER, DB_PASSWORD, WP_HOME values
3. Find and replace all strings 'http://web.cube-md.localtest.me' with your set WP_HOME value in dev_cube.sql file. 
4. Import dev_cube.sql into your database.
5. Run 'composer install' from project root folder.

If registration email is not recieved, default login can be used - 
user: janiskass
pass: ManaAtslega
