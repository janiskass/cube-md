<?php
namespace Roots\Sage\Assets;

function asset_path($filename)
{
    $dist_path = get_template_directory_uri() . '/assets/';
    $directory = dirname($filename) . '/';
    $file = basename($filename);

    return $dist_path . $directory . $file;

}

function assets()
{
    wp_enqueue_style('sage/bootstrap', get_template_directory_uri() . '/assets/styles/bootstrap.min.css', false, null);
    wp_enqueue_style('sage/css', get_template_directory_uri() . '/assets/styles/main.css', false, null);


    wp_deregister_script('jquery');

    // register the Google CDN version
    wp_register_script('jquery', asset_path('scripts/jquery-3.3.1.min.js'), array(), null, false);

    // add it back into the queue
    wp_enqueue_script('jquery');

}

add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\assets', 100);

add_action( 'login_enqueue_scripts', __NAMESPACE__ . '\\enqueue_my_script' );
function enqueue_my_script( $page ) {
    wp_enqueue_script( 'requirejs', asset_path('scripts/vendor/require.js'), null, null, true );
    wp_enqueue_script( 'my-script', asset_path('scripts/main.js'), null, null, true );
    // register the Google CDN version
    wp_register_script('jquery', asset_path('scripts/jquery-3.3.1.min.js'), array(), null, false);

    // add it back into the queue
    wp_enqueue_script('jquery');
}


function my_js_variables()
{
    ?>
    <script type="text/javascript">
        var ajaxurl = <?php echo json_encode( admin_url( "admin-ajax.php" ) ); ?>,
            base_url = <?php echo json_encode( get_template_directory_uri() ); ?>,
            template_url = <?php echo json_encode( get_stylesheet_directory_uri() ); ?>

    </script><?php
}
add_action('wp_head', __NAMESPACE__ . '\\my_js_variables');
add_action('login_head', __NAMESPACE__ . '\\my_js_variables');