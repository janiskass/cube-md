<?php

use TijsVerkoyen\Akismet\Akismet;

class Lx22gYafBeLWRAp_ajax
{
    /*
     * Define unique ajax functions prefix
     */
    private static $bothprefix = "XDKR4TQkfc";
    private static $authprefix = "RyJ4BM7UwC";
    private static $publprefix = "gQhnQu8zME";

    /*
     * Actions for both logged in and not logged in users
     */
    private $both = array(
        'check_user_email',
    );

    /*
     * Actions only for logged in users
     */
    private $auth = array();

    /*
     * Actions only for not-logged in users
     */
    private $publ = array();

    public function __construct()
    {
        /*
         * Register Actions for ajax requests
         */
        if (isset($this->both) && count($this->both) > 0) {
            foreach ($this->both as $k) {
                add_action('wp_ajax_' . $k, array($this, self::$bothprefix . '_' . $k));
                add_action('wp_ajax_nopriv_' . $k, array($this, self::$bothprefix . '_' . $k));
            }
        }

        if (isset($this->publ) && count($this->publ) > 0) {
            foreach ($this->publ as $k) {
                add_action('wp_ajax_nopriv_' . $k, array($this, self::$publprefix . '_' . $k));
            }
        }

        if (isset($this->auth) && count($this->auth) > 0) {
            foreach ($this->auth as $k) {
                add_action('wp_ajax_' . $k, array($this, self::$authprefix . '_' . $k));
            }
        }
    }


    function XDKR4TQkfc_check_user_email()
    {
        $post = $_POST;

        if (email_exists( $post['user_email'] )) {
            wp_send_json_success();
        } else {
            wp_send_json_error();
        }

    }



}

new Lx22gYafBeLWRAp_ajax();
