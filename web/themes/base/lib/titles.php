<?php

namespace Roots\Sage\Titles;

/**
 * Page titles
 */
function title() {
  if (is_home()) {
    if (get_option('page_for_posts', true)) {
      return get_the_title(get_option('page_for_posts', true));
    } else {
      return __('Latest Posts', 'sage');
    }
  } elseif (is_archive()) {
    return get_the_archive_title();
  } elseif (is_search()) {
    return sprintf(__('Search Results for %s', 'sage'), get_search_query());
  } elseif (is_404()) {
    return __('Not Found', 'sage');
  }elseif(is_account_page()){
    if(is_wc_endpoint_url('lost-password')){
      return pll__('Forgot Password?');
    }
    if(isset($_GET['action']) && $_GET['action'] == 'register'){
      return pll__('Sign Up');
    }else{
      return pll__('My Profile');
    }
  }

  else {
    return get_the_title();
  }
}
