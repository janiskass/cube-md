requirejs.config({
    deps: ['jquery'],
    baseUrl: base_url + '/assets/scripts/modules',
    //urlArgs: "v=" + (new Date()).getTime(),
    waitSeconds: 30,
    paths: {
        // plugins
        'async': '../vendor/async',
        'jquery': '../jquery-3.3.1.min',
        'modernizr' : '../vendor/modernizr-custom',

        // modules
        'login': 'account',

    },
    shim: {


    }
});

requirejs(['common'], function () {
    (function ($) {
        var UTIL = {
            loadEvents: function () {
                common.init();
                $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function (i, classnm) {
                    if (typeof( requirejs.s.contexts._.config.paths[classnm] ) != 'undefined') {
                        requirejs([classnm], function () {

                            if (typeof( this[classnm] ) !== 'undefined') {
                                if (typeof( this[classnm].finalize ) !== 'undefined') {
                                    this[classnm].finalize();
                                }
                            }
                        });
                    }
                });
            }
        };
        // Load Events
        $(document).ready(UTIL.loadEvents);
    })(jQuery); // Fully reference jQuery after this point.
});