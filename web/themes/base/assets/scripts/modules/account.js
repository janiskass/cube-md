var account = {
    init: function () {
        account.setEvents();
    },
    setEvents: function(){

        var submit_button = $('input#wp-submit');

        $( 'body' ).on( 'keyup', 'input[name=user_email]',
            function(e) {
            $.ajax({
                url: ajaxurl,
                type: 'POST',
                data: {
                    action: 'check_user_email',
                    user_email: $(this).val(),
                }
            })
                .done(function (response) {
                    console.log(response);

                    if (response.success) {
                        submit_button.attr('disabled', true).attr('title', 'Email already exists');
                    } else {
                        submit_button.attr('disabled', false).attr('title', '');
                    }
                });
            });

    }
};
account.init();
