<?php
use Roots\Sage\Setup;
use Roots\Sage\Wrapper;
?>
<!doctype html>
<html>
<?php get_template_part('templates/head'); ?>
<body <?php body_class(); ?>>
<!--[if IE]>
<div class="alert alert-warning">
    <?php echo 'You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.'; ?>
</div>
<![endif]-->
<?php
do_action('get_header');
get_template_part('templates/header');

?>
<div class="wrap container-fluid" role="document">
    <div class="content row">
        <main class="main">
            <?php
            include Wrapper\template_path();
            ?>
        </main><!-- /.main -->
    </div><!-- /.content -->
    <?php
    do_action('get_footer');

    get_template_part('templates/footer');

    wp_footer();
    ?>
</div><!-- /.wrap -->
<div id="prevent-scroll"></div>
<script async data-main="<?php echo get_template_directory_uri(); ?>/assets/scripts/main.js?v=<?php echo time(); ?>"
        src="<?php echo get_template_directory_uri(); ?>/assets/scripts/vendor/require.js?v=<?php echo time(); ?>"></script>
</body>
</html>
