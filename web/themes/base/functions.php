<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = array(
    'lib/assets.php',    // Scripts and stylesheets
    'lib/setup.php',     // Theme setup
    'lib/titles.php',    // Page titles
    'lib/wrapper.php',   // Theme wrapper class
    'lib/registration.php',   // Theme wrapper class
    'lib/ajax.php',             // Ajax actions
);

foreach ($sage_includes as $file) {
    if (!$filepath = locate_template($file)) {
        trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
    }

    require_once $filepath;
}
unset($file, $filepath);


function get_page_by_template($template, $lang = false)
{
    $args = array(
        'post_type' => 'page',
        'suppress_filters' => false,
        'orderby' => 'post_id',
        'order' => 'ASC',
        'meta_query' => array(
            array(
                'key' => '_wp_page_template',
                'value' => $template,
                'compare' => '=',
            )
        )
    );
    if ($lang != false) {
        $args['language'] = $lang;
    }
    $page = get_posts($args);
    if (!empty($page)) {
        return $page[0];
    } else {
        return get_post(get_option('page_on_front'));
    }
}





