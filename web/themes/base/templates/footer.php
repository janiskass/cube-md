<?php if( function_exists( 'pll_current_language' ) ){
    $lang = pll_current_language();
} else{
    $lang = 'en';
}
?>
<footer class="footer row">
    <div class="footer-top container">
        <div class="row">
            <div class="col-12 footer-socials text-center">
            </div>
        </div>
    </div>

    <div class="footer-bottom container">
        <div class="row">
            <div class="col text-center footer-info">
                <?php echo "Jānis Kašs, ".date('Y') ;?>
            </div>
        </div>
    </div>

</footer>