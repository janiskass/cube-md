<header class="banner no-bg">
	<div class="container header-inner clearfix">
		<div class="brand">
			<a href="<?php echo esc_url(home_url()); ?>/">
                RSS FEED
			</a>
		</div>
		<div class="menu-outer">
			<div class="header-top row">
				<ul>

				</ul>
			</div>
			<nav class="nav-main row" id="header-navigation">
                <ul class="nav">
                    <li>
                       <?php wp_loginout(); ?>
                    </li>
                </ul>
			</nav>
			<?php if(function_exists('pll_the_languages')) : ?>
			<?php endif; ?>
		</div>
		<a href="#" class="main-menu-toggle">
			<span class="burger"></span>
		</a>
	</div>
</header>