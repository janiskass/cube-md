<?php
/**
 * Template Name: Login
 */
?>
<div class="container">
    <div class="row">
        <div class="col-12 field-container">
            <?php
            if(!is_user_logged_in()) {
                wp_login_form();
            }else{ ?>
                <a href="<?php echo wp_logout_url();?>">Logout</a>
            <?php }
                wp_register();
            ?>
        </div>
    </div>
</div>
