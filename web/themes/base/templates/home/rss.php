<div class="container page-content no-header">
<?php
if(!is_user_logged_in()): ?>
    <div class="row">
        <div class="col-12">
            <div class="alert alert-danger">
                You need to log in to view RSS feed!
            </div>
        </div>
    </div>
<?php else:
    $feed = array();
    $rss = new DOMDocument();
    $rss->load('https://www.tvnet.lv/rss');


    for ($i = 0 ; $i<5; $i++){
        $node = $rss->getElementsByTagName('item')[$i];
        $item = array (
            'title' => $node->getElementsByTagName('title')->item(0)->nodeValue,
            'description' => $node->getElementsByTagName('description')->item(0)->nodeValue,
            'link' => $node->getElementsByTagName('link')->item(0)->nodeValue,
            'image' => $node->getElementsByTagName('enclosure')->item(0)->getAttribute('url'),
        );
        array_push($feed, $item);
    }
    ?>
    <div class="row">
        <?php foreach($feed as $item) : ?>
            <div class="col col-4 feed-item">
                <div class="image" style="background-image: url('<?php echo $item['image'];?>')">
                </div>
                <h3 class="title"><a href="<?php echo $item['link'];?>" target="_blank"><?php echo $item['title'] ;?></a></h3>
                <div class="description"><?php echo $item['description'] ;?></div>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>

</div>
